<?xml version="1.0" encoding="ISO-8859-1"?>
<appendix id="appendix_a">
  <title>Useful Memory Terms</title>

  <remark>Change the title to something better</remark>

  <section id="buffer_overflow">
    <title>Buffer overflow</title>

    <para>A buffer overflow occurs when a program or process attempts to write
    more data to a fixed length block of memory, or buffer, than the buffer is
    allocated to hold.</para>

    <para>Since buffers are created to contain a defined amount of data, the
    extra data can overwrite data values in memory addresses adjacent to the
    destination buffer, unless the program includes sufficient bounds checking
    to flag or discard data when too much is sent to a memory buffer.
    Languages such Ada, C#, Haskell, Java, JavaScript, Lisp, PHP, Python,
    Ruby, and Visual Basic enforce run-time checking. </para>

    <para>C and C++ are prone to buffer overflow attacks as they have no
    built-in protection against accessing or overwriting data in any part of
    their memory, and do not automatically check that data written to an array
    (the built-in buffer type) is within the boundaries of that array.</para>
  </section>

  <section id="security_exploit">
    <title>Security Exploit</title>

    <para>On many systems, the memory layout of a program, or the system as a
    whole, is well defined. Exploiting the behavior of a buffer overflow is a
    well-known security exploit. By sending in data designed to cause a buffer
    overflow, it is possible to write into areas known to hold an executable,
    and replace it with malicious code.</para>

    <para>Bounds checking can prevent buffer overflows, but requires
    additional code and processing time. Modern operating systems use a
    variety of techniques to combat malicious buffer overflows by randomizing
    the layout of memory, or deliberately leaving space between buffers and
    looking for actions that write into those areas "canaries". For further
    reading about security exploits refer to the <ulink
    url="https://www.exploit-db.com/">Exploit database</ulink>.</para>
  </section>

  <section id="hd_bounds_ck">
    <title>Hardware Bounds Checking</title>

    <para>The safety added by bounds checking costs CPU time if checking is
    performed in software however, if the checks are performed by hardware
    then safety can be provided "for free" with no runtime cost. </para>

    <para>Research was started at least as early as 2005 regarding methods to
    use x86's built-in virtual memory management unit to ensure safety of
    array and buffer accesses. In 2015 Intel provided their Intel MPX
    extensions in their Skylake processor architecture which stores bounds in
    a CPU register and table in memory. As of early 2017 GCC among others,
    supports MPX extensions.</para>
  </section>

  <section id="rootkit">
    <title>Rootkit</title>

    <para>A rootkit is a collection of malicious software designed to enable
    access to another software that would not otherwise be allowed, e.g: to an
    unauthorized user, and often masks its existence or the existence of other
    software. The term rootkit is a concatenation of "root" and
    the word "kit", which refers to the software components that implement the
    tool, and is associated with malware.</para>
  </section>
</appendix>