# To be included in Makefile
# NOTE: MANIFESTHASH needs to be changed to final release tag in format refs/tags/ELnnn before a release
#       The values are shown in the release info
#       The manifest is used to fetch information into the release info from the distro files
#MANIFESTHASH      ?= 0d0f06384afa65eaae4b170e234ee5a813edf44d
	#change the above value later to refs/tags/ELnnn (?)
MANIFESTURL       := git@git.enea.com:linux/manifests/el_manifests-virtualization.git
PROFILE_NAME      := Enea NFV Access
