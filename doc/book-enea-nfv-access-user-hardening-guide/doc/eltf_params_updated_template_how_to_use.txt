eltf_params_template_updated_howto_use.txt

This is a way to collect all parameters for an Enea NFV Access release
in one parameter file, easy to automatically update by ELTF regularly.

NOTE: Both the release info AND the open source books use parameters from
      here, but the XML file is inside the release info book directory.

NOTE: The manifest_conf.mk, or overridden by the environment variable
      MANIFESTHASH, contains the full tag (or hashvalue) for downloading
      the manifest when the books are built. The list of target
      directories are fetched from the manifest into the book.
      The eltf_params_updates.xml can all the time contain
      the final next complete tag e.g. refs/tags/EL6 or similar
      in the ELTF_PL_CLONE_W_REPO parameter command lines.

The ordinary book XML files use xi:include statements to include elements
from this parameter file. The book XML files can thus be manually edited.
Before editing, you must run "make init".
Any other text in the template or updated.xml file, outside the parts that
are included in the book, are not used but still all must be correct
DocBook XML files.

ELTF work:
 template => ELTF replaces ALL ELTF_xxx variables => updated XML file
    => push to git only if changed


eltf_params_template.xml  (in git)
   File used by ELTF to autocreate/update the real parameter
   file eltf_params_updated.xml.

eltf_params_updated.xml  (in git)
   Real parameter file where ELTF has replaced all ELTF_xx variables with
   strings, in several cases with multiline strings.
   No spaces or linefeed allowed in beginning or end of the variable values!


xi:include: Each parameter is xi:include'ed in various book files, using
   the IDs existing in the parameter files.
   In most cases the 1:st element inside an element with an ID is included
   using a format like eltf-prereq-apt-get-commands-host/1.
   In very few cases the element with the ID is included in the book, one
   example is the target section which has an ID, but which contains
   multiple subsections, one per target.
   All IDs in a book must be unique.

DocBook XML: All XML files must be correct DocBook XML files.

Do NOT edit/save the real *updated.xml file with XMLmind to avoid changes
   not done by ELTF. But it is OK to open the real file in XMLmind to
   check that the format is correct.

ELTF should autocreate a temporary "real" file but only replace
   and push the eltf_params_updated.xml if it is changed.


make eltf
   This lists all ELTF_xxx variables and some rules how to treat them

DocBook Format: All elements - rules:
   Several strict generic XML rules apply for all strings:
    1. No TABs allowed or any other control chr than "linefeed"
    2. Only 7-bit ASCII
    3. Any < > & must be converted to &lt; &gt; and &amp;
       Similar for any other non-7-bit-ASCII but avoid those!
    4. No leading spaces or linefeeds when replacing the ELTF_* variable
    5. No trailing spaces or linefeeds when replacing the ELTF_* variable
    6. Note: Keep existing spaces before/efter ELTF_* in a few cases.

DocBook Format: <programlisting> - rules: ELTF*PL* variables
   Several strict rules apply for the multiline string in programlisting
   in addition to the general XML rules above:
    7. Max line length < 80 char
    8. Use backslash (\) to break longer lines
    9. Use spaces (e.g. 4) to indent continuation lines in programlistings
   10. No trailing spaces on any line
   11. No spaces or linefeed immediately after leading <programlisting>
   12. No spaces or linefeed before trailing </programlisting>

DocBook Format: <ulink> - rules: ELTF_*URL* variables
   13. ELTF_*URL and corresponding ELTF_*TXTURL shall be identical strings
   14. Only if the URL is extremely long, the TXTURL can be a separate string

Each target has one section with target parameters:
       <section id="eltf-target-table-ELTF_T_MANIFEST_DIR">
        <title>MACHINE ELTF_T_MANIFEST_DIR - Information</title>
          ..... with many ELTF_ variables ....
       </section>

   15. If there is only one target. ELTF just replaces ELTF parameters

   16. It there are multiple targets. ELTF copies the section and appends the
       section the required number of times.
       Each section ID will become unique: eltf-target-table-ELTF_T_MANIFEST_DIR
       Each section title will become unique

Tables with target parameters in each target section:
   17. It is possible for ELTF to append more rows with one parameter each
       to these tables, because the entire tables are included in the book

Special - NOT YET READY DEFINED how to handle the optionally included
   Eclipse and its version, but this is a first suggestion:
   18. Just now ELTF can define ELFT_ECLIPSE_VERSION as a full string
       with both version number and name,
   19. MANUALLY if Eclipse is NOT included in the release,
       the release manager should manually set condition="hidden" on
       the entire section in the book XML about Eclipse



BELOW WE TRY TO EXPLAIN EACH ELTF_* variable, but always check with make eltf
if there are more new variables, missing in this description file.

_____________________________________________________________________________
ELTF_PL_HOST_PREREQ       Multiline list of host prerequisites, e.g. commands
                          like sudo apt-get install xxxx or similar.
                          First line = comment with the complete host name!
                          It is possible to include multiple hosts by just
                          adding an empty line, comment with host name, etc.
                          xi:include eltf-prereq-apt-get-commands-host/1
                          This is a <programlisting>...</programlisting>
                          Example:
# Host Ubuntu 14.04.5 LTS 64bit
sudo apt-get update
sudo apt-get install sed wget subversion git-core coreutils unzip texi2html \
   texinfo libsdl1.2-dev docbook-utils fop gawk python-pysqlite2 diffstat \
   make gcc build-essential xsltproc g++ desktop-file-utils chrpath  \
   libgl1-mesa-dev libglu1-mesa-dev autoconf automake groff libtool xterm \
   libxml-parser-perl

_____________________________________________________________________________
ELTF_PL_GET_REPO          Multiline commands to download the repo tool
                          xi:include eltf-getting-repo-install-command/1
                          This is a <programlisting>...</programlisting>
                          Example:
mkdir -p ~/bin
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/bin/repo
chmod a+x ~/bin/repo
export PATH=~/bin:$PATH

_____________________________________________________________________________
ELTF_EL_REL_VER           General parameter string: The version of this Enea
                          Linux release. Major version and optional .Minor
                          Typically created from MAJOR and MINOR in enea.conf
                          MINOR in enea.conf is empty or contains a dot+minor
                          xi_include EneaLinux_REL_VER/1
                          This is a <phrase>X.x</phrase> used in many places.
                          Examples:
6
  or
6.1

_____________________________________________________________________________
ELTF_YOCTO_VER            General parameter string: Yocto version, created
                          from DISTRO in poky.ent
                          xi:include Yocto_VER/1
                          This is a <phrase>X.x</phrase> used in many places.
                          Example:
2.1

_____________________________________________________________________________
ELTF_YOCTO_NAME           General parameter string: Yocto name (branch), created
                          from DISTRO_NAME_NO_CAP in poky.ent
                          xi:include Yocto_NAME/1
                          This is a <phrase>X.x</phrase> used in many places.
                          Example:
krogoth

_____________________________________________________________________________
ELTF_YOCTO_PROJ_DOWNLOAD_TXTURL  General parameters. These two are IDENTICAL
ELTF_YOCTO_PROJ_DOWNLOAD_URL     strings with correct Yocto version string
                                 at the end, typically without "dot".
                          xi:include ULINK_YOCTO_PROJECT_DOWNLOAD/1
                          This is an <ulink url="...">...</ulink>
                          Example:
http://www.yoctoproject.org/downloads/core/krogoth/21

_____________________________________________________________________________
ELTF_EL_DOWNLOAD_TXTURL   General parameters. These two are IDENTICAL strings
ELTF_EL_DOWNLOAD_URL      and shall be the http:/..... address where
                          Enea NFV Access can be downloaded
                          Often containing same version as in ELTF_EL_REL_VER
                          xi:include ULINK_ENEA_LINUX_URL/1
                          This is an <ulink url="...">...</ulink>
                          Example:
http://linux.enea.com/6

_____________________________________________________________________________
ELTF_PL_CLONE_W_REPO      Multiline commands to run repo to clone everything.
                          Use the variable $MACHINE/default.xml (the text in
                          the book will list the avaiable values of MACHINE,
                          taken from the manifest repository)
                          xi:include eltf-repo-cloning-enea-linux/1
                          This is a <programlisting>...</programlisting>
                          Example:
mkdir enea-linux
cd enea-linux
repo init -u git://git.enea.com/linux/el_manifests-standard.git \
    -b refs/tags/EL6 -m $MACHINE/default.xml
repo sync

_____________________________________________________________________________
ELTF_ECLIPSE_VERSION      Optional general parameter string.
                          NOT YET READY DEFINED
                          Just now a release manage must manually set
                          condition="hidden" on the Eclipse section,
                          if Eclipse is not included in the release.
                          ELTF just replaces ELTF_ECLIPSE_VERSION with a full
                          string with "X.Y (name)"
                          It includes the ID and can only be ONCE in the book.
                          xi:include eltf-eclipse-version-row
                          Example.
4.5 (Mars)


_____________________________________________________________________________
ELTF_T_*                  All these are in each target (MACHINE) and ELTF
                          must separately replace them with strings for
                          each target
                          NOTE: All (except the MANIFEST_DIR) are in rows
                                in a table and ELTF can select to append
                                more parameters by adding more rows

_____________________________________________________________________________
ELTF_T_MANIFEST_DIR       This happens to be in two places. Must be exactly
ELTF_T_MANIFEST_DIR       the directory name in the manifest, e.g. same
                          as the MACHINE names in $MACHINE/default.xml.
                          In book: a) Part of section ID
                                   b) Part of section title
                          Examples:
p2041rgb
  or
ls1021aiot
  or
qemuarm

_____________________________________________________________________________
ELTF_T_NAME               Target specific: "Target Official Name"
                          NOT same as the target directory name in most cases.
                          In book: An <entry> element in a row
                          Examples:
P2041RGB
  or
LS1021a-IoT
  or
qemuarm

_____________________________________________________________________________
ELTF_T_ARC_DESC           Target specific: "Architecture and Description"
                          It can be a short identification string or
                          it can be a longer descriptive sentence.
                          In book: An <entry> element in a row
                          Examples:
Power, e500mc
  or
ARM Cortex-A7

_____________________________________________________________________________
ELTF_T_DS_TXTURL          Target specific: "Link to target datasheet. These
ELTF_T_DS_URL             two usually are IDENTICAL strings with correct
                          hyperlink to the target's official datasheet.
                          In book: an <ulink url="...">...</ulink>
                          Only if the link is VERY LONG, the text part shall
                          instead be a descriptive string (see 2:nd example).
                          NOTE: Also here no spaces or line-feeds!
                          Examples:
url="http://wiki.qemu.org">http://wiki.qemu.org
or
url="http://www.nxp.com/products/microcontrollers-and-processors/arm-processors/qoriq-arm-processors/qoriq-ls1021a-iot-gateway-reference-design:LS1021A-IoT">link to NXP's datasheet

_____________________________________________________________________________
ELTF_T_POKY_VER           Target specific: "Poky version" created either
                          from POKYVERSION in poky.ent
                          or using a hashvalue with a leading string, in
                          which case it may be different per target.
                          In book: An <entry> in a row
                          Examples:
15.0.0
or
Git commit id: 75ca53211488a3e268037a44ee2a7ac5c7181bd2

_____________________________________________________________________________
ELTF_T_GCC_VER            Target specific: "GCC Version". Should be in poky
                          but not easy to find among various parameters.
                          ELTF would extract it from build logs building SDK
                          and it is possibly different per target.
                          In book: An <entry> in a row
                          Example:
5.3

_____________________________________________________________________________
ELTF_T_KERN_VER           Target specific: "Linux Kernel Version". Often
                          different per target.
                          In book: An <entry> in a row
                          Example:
3.12

_____________________________________________________________________________
ELTF_T_DRIVERS            Target specific: "Supported Drivers". This is a
                          comma-separated list of driver names.
                          ELTF should create the list in same order for each
                          target, e.g. alphabetic migth be OK.
                          In book: An <entry> in a row
                          Example:
Ethernet, I2C, SPI, PCI, USB, SD/SDHC/SDXC


_____________________________________________________________________________
ELTF_T_EL_RPM_TXTURL      Target specific: "Enea rpm folder for downloading
ELTF_T_EL_RPM_URL         RPM packages for this target". These two are
                          INDENTICAL strings with hyperlink to the web site
                          at Enea where the customer can download RPMs
                          Note: Often the ELFT_EL_REL_VER value and
                                the ELTF_T_MANIFEST_DIR are used in the link.
                          In book: an <ulink url="...">...</ulink>
                          Example:
url="https://linux.enea.com/6/ls1021aiot/rpm">https://linux.enea.com/6/ls1021aiot/rpm

_____________________________________________________________________________
