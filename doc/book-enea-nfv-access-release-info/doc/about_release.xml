<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.2//EN"
"http://www.oasis-open.org/docbook/xml/4.2/docbookx.dtd">
<chapter id="relinfo-about-release">
  <title>About This Release</title>

  <para>This release of Enea NFV Access <xi:include
  href="eltf_params_updated.xml" xmlns:xi="http://www.w3.org/2001/XInclude"
  xpointer="element(EneaLinux_REL_VER/1)" /> provides a publicly available
  Enea NFV Access distribution for a selected set of targets.<remark>INFO
  <emphasis role="bold">eltf_params_updated.xml</emphasis> contains many
  parameters in the book, also in the open source books, and MUST be created
  FROM <emphasis role="bold">eltf_params_template.xml</emphasis>. The
  parameters are automatically used in the books, via xi:include statements,
  similar to how parameters from pardoc-distro.xml are included in the book.
  Read the file <emphasis
  role="bold">eltf_params_updated_template_how_to_use.txt</emphasis> for
  important details about formats and how to do! The idea is that ELTF will
  auto-create/update it.</remark></para>

  <section id="release-content">
    <title>NFV Access Release content</title>

    <para>The current release contains along with other items, documentation,
    pre-built kernels and images, a bootloader and a SDK. The directories
    structure is detailed below:</para>

    <programlisting>-- documentation/
   /* NFV Access documentation */
-- inteld1521/
   /* artifacts for the host side */
        -- deb/
           /* deb packages */
        -- images/
            -- enea-image-virtualization-host
               /* precompiled artifacts for the Host release image */
                -- various artifacts
            -- enea-image-virtualization-host-sdk
               /* precompiled artifacts for the Host SDK image.
               The SDK image contains userspace tools and kernel
               configurations necessary for developing, debugging
               and profiling applications and kernel modules */
                -- various artifacts
        -- sdk
           /* NFV Access SDK for the host */
                -- enea-glibc-x86_64-enea-image-virtualization-host-sdk /
                   -corei7-64-toolchain-7.0.sh
                   /* self-extracting archive installing
                   cross-compilation toolchain for the host */
-- qemux86-64
   /* artifacts for the guest side */
        -- deb/
           /* deb packages */
        -- images/
            -- enea-image-virtualization-guest
               /* precompiled artifacts for the Guest image */
                -- various artifacts
        -- sdk
           /* NFV Access SDK for the guest */
                -- enea-glibc-x86_64-enea-image-virtualization-guest-sdk /
                   -core2-64-toolchain-7.0.sh
                   /* self-extracting archive installing cross-compilation
                   toolchain for the guest (QEMU x86-64) */
</programlisting>

    <para>For each combination of image and target, the following set of
    artifacts is available:</para>

    <programlisting>-- bzImage
   /* kernel image */
-- bzImage-&lt;target&gt;.bin
   /* kernel image, same as above */
-- config-&lt;target&gt;.config
   /* kernel configuration file */
-- core-image-minimal-initramfs-&lt;target&gt;.cpio.gz
   /* cpio archive of the initramfs */
-- core-image-minimal-initramfs-&lt;target&gt;.qemuboot.conf
   /* qemu config file for the initramfs image */
-- &lt;image-name&gt;-&lt;target&gt;.ext4
   /* EXT4 image of the rootfs */
-- &lt;image-name&gt;-&lt;target&gt;.hddimg
   /* msdos filesystem containing syslinux, kernel, initrd and rootfs image */
-- &lt;image-name&gt;-&lt;target&gt;.iso
   /* CD .iso image */
-- &lt;image-name&gt;-&lt;target&gt;.qemuboot.conf
   /* qemu config file for the image */
-- &lt;image-name&gt;-&lt;target&gt;.tar.gz
   /* tar archive of the image */
-- &lt;image-name&gt;-&lt;target&gt;.wic
   /* Wic image */
-- microcode.cpio
   /* kernel microcode data */
-- modules-&lt;target&gt;.tgz
   /* external kernel modules */
-- ovmf.*.qcow2
   /* ovmf firmware for uefi support in qemu */
-- rmc.db
   /* Central RMC Database */
-- systemd-bootx64.efi
   /* systemd-boot EFI file */
-- grub-efi-bootx64.efi
   /* GRUB EFI file */</programlisting>
  </section>

  <section id="relinfo-supported-host-environment">
    <title>Supported Host Environment</title>

    <itemizedlist>
      <para>The following environments have been validated as host
      environments for Enea NFV Access 1.0:</para>

      <listitem>
        <para>Ubuntu 16.04 LTS, 64bit<remark>Hardcoded now in this XML file.
        Might be moved to the parameter file later.</remark><remark>INFO Below
        is a complete section with ID "eltf-target-tables-section" included
        from elft_params_updated.xml. It contains a variable number of target
        tables and the section title should be "Supported Targets with
        Parameters". It has have a short sentence about what it is in the
        beginning. The subtitles shall have the directory name of the target
        in the manifest.</remark></para>
      </listitem>
    </itemizedlist>

    <para>For more details on system requirements and how to configure the
    host environment, please see <xref
    linkend="sys-req-prerequisites" />.</para>
  </section>

  <xi:include href="eltf_params_updated.xml"
              xmlns:xi="http://www.w3.org/2001/XInclude"
              xpointer="element(eltf-target-tables-section)" />

  <section id="relinfo-provided-sdk">
    <title>Provided Toolchain(s) (SDK)</title>

    <para>The SDK contains toolchains supporting cross-compilation of
    applications for the targets on an x86_64b host. <remark>(Possibly add
    this in later) See the <trademark class="registered">Enea</trademark> NFV
    Access Application Development Guide for information on how to build and
    install a toolchain.</remark></para>
  </section>

  <section id="relinfo-documentation">
    <title>Provided Documentation</title>

    <para>Enea NFV Access is provided with the following set of
    documents:</para>

    <itemizedlist spacing="compact">
      <listitem>
        <para>Enea NFV Access Release Information &ndash; This document,
        describing the Enea NFV Access release content.</para>
      </listitem>

      <listitem>
        <para>Enea NFV Access Guide &ndash; A document describing how to use
        Enea NFV Access, as well as use cases and benchmark results.</para>
      </listitem>

      <listitem>
        <para>Enea NFV Access Open Source Report &ndash; A document containing
        the open source and license information pertaining to packages
        provided with Enea NFV Access.</para>
      </listitem>

      <listitem>
        <para>Enea NFV Access Test Report &ndash; The document that summarizes
        the test results for the Enea NFV Access release.</para>
      </listitem>

      <listitem>
        <para>Enea NFV Access Security Report &ndash; The document that lists
        all security fixes included in the Enea NFV Access release.</para>
      </listitem>
    </itemizedlist>
  </section>

  <xi:include href="../../s_doceneacommon/doc/security_fixes.xml"
              xmlns:xi="http://www.w3.org/2001/XInclude" />
</chapter>