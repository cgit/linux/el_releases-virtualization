# Component build specification

# Version of THIS book
BOOK_VER ?= $(REL_VER)-dev

DOCBOOK_SRC := $(COMP)/swcomp.mk $(COMP)/doc/book.xml $(shell find $(COMP)/doc -type f \( -name "*.xml" -o -name "*.svg" -o -name "*.png" \) ! -name "book.xml" -print)

BOOKPACKAGES := book-enea-nfv-access-release-info
BOOKDESC_$(BOOKPACKAGES) := "Enea NFV Access $(PROD_VER) Release Information"
BOOKDEFAULTCONDITION := $(DEFAULTCONDITIONS)
